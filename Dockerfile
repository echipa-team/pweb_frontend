FROM node:14.19.1-buster

WORKDIR /frontend

COPY ./package.json /frontend
COPY /public /frontend/public
COPY /src /frontend/src
COPY /cert /frontend/cert

RUN npm install
RUN npm install @auth0/auth0-react
RUN npm install @mui/material @emotion/react @emotion/styled @mui/icons-material @mui/x-date-pickers date-fns
RUN npm install react-router-dom@6
RUN npm install react-bootstrap bootstrap react-router-bootstrap 
RUN npm install react-bootstrap-datetimepicker
RUN npm install react-phone-number-input

CMD [ "npm", "start" ]
