import { useAuth0 } from "@auth0/auth0-react"

export default function UserProfile() {
  const { user, isAuthenticated, isLoading } = useAuth0()

  if (isLoading) {
    return <div>Loading</div>
  }
  
  console.log(isAuthenticated)
  return (
    isAuthenticated ? (
      <div>
        <h2>yaya</h2>
        <h2>img src={user.picture}</h2>
        <h2>{user.name}</h2>
        <h2>{user.email}</h2>
      </div>
    ) : <div>nope</div>
    
  )
}