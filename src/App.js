import "./App.css";
import React from "react";
import Router from "./Router";
import Wrapper from "./auth/AuthWrapper";

const App = () => {
  return (
    <Wrapper>
      <Router />
    </Wrapper>
  );
};

export default App;
