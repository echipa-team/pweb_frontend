import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { styled } from '@mui/material/styles'
import { useAuth0 } from "@auth0/auth0-react"
import { Nav } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import Button from '@mui/material/Button';
import SplitButton from '../common/SplitButton'
import SendIcon from '@mui/icons-material/Send'

import RequestsNavbar from './../common/RequestsNavbar'

const RootNotSelected = styled('div')(({ theme }) => ({
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  transition: '0.3s',
  width: '75%',
  transform: 'translateX(20%)',
  marginTop: '200px',
  padding: '20px',
  paddingBottom: '25px',
  display: 'block',
  textAlign: 'center',
  fontSize: '2rem',
}))

const Refugee = styled('div')(({ theme }) => ({

}))

const ConfigureCity = styled('div')(({ theme }) => ({
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  transition: '0.3s',
  width: '75%',
  transform: 'translateX(20%)',
  marginTop: '200px',
  padding: '20px',
  paddingBottom: '25px',
  display: 'block',
  textAlign: 'center',
  fontSize: '2rem',
}))

const profileStyle = {
  color: '#0000EE',
  textDecoration: 'underline'
}

const createReqBtn = {
  display: 'block',
  height: '50px',
  width: '20%',
  fontSize: '2rem',
  color: 'white',
  background: 'linear-gradient(90deg, rgba(209,55,242,0.6615021008403361) 0%, rgba(195,49,213,0.6615021008403361) 33%, rgba(179,107,237,0.6502976190476191) 50%, rgba(40,151,205,0.5914740896358543) 66%, rgba(5,217,230,0.7399334733893557) 100%)',
  marginTop: '20px',
  transform: 'translateX(200%)',
  borderRadius: '20px',
  paddingTop: '0px',
}

export default function Requests() {
  const { getAccessTokenSilently } = useAuth0()
  const [accessToken, setAccesToken] = useState()
  const [isUser, setIsUser] = useState({})
  const [userType, setUserType] = useState()
  const [locationsFromCity, setLocationsFromCity] = useState({})
  const [myRequests, setMyRequests] = useState()

  useEffect(() => {
    if (typeof accessToken === 'undefined') {
      (async () => {
        const access_token = await getAccessTokenSilently();
        setAccesToken(access_token);
        fetch(`https://pwebgroup.tech:8080/api/is_user?access_token=` + access_token,
          {
            method: 'GET',
            dataType: 'json',
          }).then(function (response) {
            return response.json();
          }).then((actualData) => { setIsUser(actualData) })
      })();
    }
  }, [accessToken, getAccessTokenSilently]);

  const handleUserType = (type) => {
    setUserType(type)
  }

  const onSendTypeHandle = () => {
    const jsonToSend = { access_token: accessToken, type: userType.toLowerCase() }
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=UTF-8' },
      body: JSON.stringify(jsonToSend)
    }
    fetch('https://pwebgroup.tech:8080/api/create_user_entry/', requestOptions)
  }

  const getUserType = () => {
    if (typeof userType === 'undefined' && typeof accessToken !== 'undefined') {
      (async () => {
        fetch('https://pwebgroup.tech:8080/api/get_user_type/?access_token=' + accessToken,
        {
          method: 'GET',
          headers: { 
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
          },
        })
          .then(function (response) {return  response.json()})
          .then((actualData) => {setUserType(actualData.data)})
      })()    
    }
  }

  const getLocationsFromCity = () => {
    if (typeof locationsFromCity.data === 'undefined') {
      (async () => {
        fetch('https://pwebgroup.tech:8080/api/get_locations_from_city?access_token=' + accessToken,
          {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
          })
          .then(function (response) { return response.json() })
          .then((actualData) => { setLocationsFromCity(actualData) })
          .then(() => { return true })
      })()
    }
  }

  const getMyRequests = () => {
    if (typeof locationsFromCity.data === 'undefined') {
      (async () => {
        fetch('https://pwebgroup.tech:8080/api/get_my_requests?access_token' + accessToken,
          {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
          })
          .then(function (response) { return response.json() })
          .then((actualData) => { setMyRequests(actualData.data) })
          .then(() => { return true })
      })()
    }
  }

  return (
    isUser ?
      isUser.data === false
        ? <RootNotSelected>
          Are you a refugee or helper ? <SplitButton handleUserType={handleUserType} />
          <div>
            <Link to='/requests'><Button variant="contained" onClick={onSendTypeHandle}>Send <SendIcon style={{ marginLeft: '20px' }} /></Button></Link>
          </div>
        </RootNotSelected>
        : getUserType() || (userType === "helper" || userType === "Helper")
          ? getLocationsFromCity() || locationsFromCity.data === 'Please configure city'
            ? <ConfigureCity>Please complete your city in {console.log(locationsFromCity)}
                <LinkContainer to="/profile" style={profileStyle}><Nav.Link>Profile</Nav.Link></LinkContainer>
              </ConfigureCity>
            : <RequestsNavbar />

          : getLocationsFromCity() || locationsFromCity.data === 'Please configure city'
            ? <ConfigureCity>Please complete your city in {console.log(locationsFromCity)}
                <LinkContainer to="/profile" style={profileStyle}><Nav.Link>Profile</Nav.Link></LinkContainer>
              </ConfigureCity>
            : getMyRequests() || (myRequests && Object.keys(myRequests).length == 0)
              ? <Refugee>
                  <Link to="/offers/create_request" style={{textDecoration: 'none'}}><Button style={createReqBtn}>Create Request</Button></Link>
                  You don't have any requests
                </Refugee>
              : <Refugee>
                  <Link to="/offers/create_request" style={{textDecoration: 'none'}}><Button style={createReqBtn}>Create Request</Button></Link>
                </Refugee>
      : <div></div>

  )
}