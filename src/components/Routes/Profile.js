import React, { useState, useEffect } from 'react'
import { styled } from '@mui/material/styles'
import { TextField, Button, Switch } from '@mui/material'
import { useAuth0 } from "@auth0/auth0-react"
import PhoneInput from 'react-phone-number-input'
import SendIcon from '@mui/icons-material/Send'
import PersonIcon from '@mui/icons-material/Person'
import { Link } from 'react-router-dom';

import 'react-phone-number-input/style.css'

const Root = styled('div')(({ theme }) => ({
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  transition: '0.3s',
  width: '25%',
  transform: 'translateX(150%)',
  marginTop: '100px',
  padding: '20px',
  paddingBottom: '50px',
  display: 'block',
}))

const Title = styled('div')(({ theme }) => ({
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  transition: '0.3s',
  width: '75%',
  height: '75px',
  transform: 'translate(20%, -70%)',
  textAlign: 'center',
  fontSize: '50px',
  color: '#F5F0E7',
  background: 'linear-gradient(90deg, rgba(209,55,242,0.6615021008403361) 0%, rgba(195,49,213,0.6615021008403361) 33%, rgba(179,107,237,0.6502976190476191) 50%, rgba(40,151,205,0.5914740896358543) 66%, rgba(5,217,230,0.7399334733893557) 100%)',
  borderRadius: '20px',
}))

const Subscribe = styled('div')(({ theme }) => ({

}))

const boxesStyle = {
  marginTop: '15px',
  marginBottom: '15px',
}

const sendBtnStyle = {
  display: 'block',
  height: '50px',
  width: '50%',
  fontSize: '2rem',
  background: 'linear-gradient(90deg, rgba(209,55,242,0.6615021008403361) 0%, rgba(195,49,213,0.6615021008403361) 33%, rgba(179,107,237,0.6502976190476191) 50%, rgba(40,151,205,0.5914740896358543) 66%, rgba(5,217,230,0.7399334733893557) 100%)',
  marginTop: '20px',
  transform: 'translateX(50%)',
  borderRadius: '20px',
  paddingTop: '0px',
}


export default function Profile() {
  const { getAccessTokenSilently } = useAuth0()
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [phoneValue, setPhoneValue] = useState('')
  const [cityValue, setCityValue] = useState('')
  const [subscribeValue, setSubscribeValue] = useState()
  const [accessToken, setAccesToken] = useState()

  useEffect(() => {
    if (typeof accessToken === 'undefined') {
      (async () => {
        const access_token = await getAccessTokenSilently();
        setAccesToken(access_token);
        const requestOptions = {
          method: 'GET',
          headers: { 
            'Content-Type': 'application/json',
         },
        }
        fetch('https://pwebgroup.tech:8080/api/get_subscription_details?access_token=' + access_token, requestOptions)
          .then(function (response) { return response.json() })
          .then((actualData) => {
            setFirstName(actualData.data.first_name !== null ? actualData.data.first_name : '')
            setLastName(actualData.data.last_name !== null ? actualData.data.last_name : '')
            setPhoneValue(actualData.data.phone_number !== null ? actualData.data.phone_number : '')
            setCityValue(actualData.data.city !== null ? actualData.data.city : '')
            setSubscribeValue(actualData.data.status)
          })
      })();
    }
  }, [accessToken, getAccessTokenSilently]);

  const onFirstNameChange = (event) => {
    setFirstName(event.target.value)
  }

  const onLastNameChange = (event) => {
    setLastName(event.target.value)
  }

  const onPhoneValueChange = (newPhoneValue) => {
    setPhoneValue(newPhoneValue)
  }

  const onCityValueChange = (event) => {
    setCityValue(event.target.value)
  }

  const onSubscribeChecked = (event) => {
    setSubscribeValue(event.target.checked)
  }

  const onSendHandle = () => {
      const jsonToSend = { "access_token": accessToken, "first_name": firstName, "last_name": lastName, "phone_number": phoneValue, "city": cityValue, "status": subscribeValue }
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json; charset=UTF-8' },
        body: JSON.stringify(jsonToSend)
      }
      fetch('https://pwebgroup.tech:8080/api/update_subscription/', requestOptions)
  }

  const GreenSwitch = styled(Switch)(({ theme }) => ({
    '& .MuiSwitch-switchBase.Mui-checked': {
      color: '#05d9e6ba ',
      '&:hover': {
        backgroundColor: ('#F5F5F5', theme.palette.action.hoverOpacity),
      },
    },
    '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
      backgroundColor: '#d137f2d9',
    },
  }));

  return (
    <Root>
      <Title><PersonIcon style={{ height: '50px', width: '50px', paddingBottom: '5px' }} /> Profile</Title>
      <TextField id="outlined-basic" label="First Name" 
          variant="outlined" style={boxesStyle} onChange={onFirstNameChange} 
          value={firstName}
      />
      <TextField id="outlined-basic" label="Last  Name" 
          variant="outlined" style={boxesStyle} onChange={onLastNameChange} 
          value={lastName}
      />
      <PhoneInput
        placeholder="Enter phone number without prefix"
        style={boxesStyle}
        defaultCountry="US"
        value={phoneValue}
        onChange={onPhoneValueChange}
      />
      <TextField id="outlined-basic" label="City" variant="outlined"
        style={boxesStyle} onChange={onCityValueChange}
        value={cityValue}
      />
      <Subscribe>Subscribe<GreenSwitch onChange={onSubscribeChecked} checked={subscribeValue}
      /> </Subscribe>
      <Link to="/offers" style={{textDecoration: 'none'}}><Button variant="contained" style={sendBtnStyle} onClick={onSendHandle}>Send <SendIcon /></Button></Link>
    </Root>
  )
}