import React, { useState, useEffect } from 'react'
import { styled } from '@mui/material/styles'
import { useAuth0 } from "@auth0/auth0-react"
import Button from '@mui/material/Button';
import SplitButton from '../common/SplitButton'
import SendIcon from '@mui/icons-material/Send'
import { Link } from 'react-router-dom'

import HomeWalp from './../../assets/homewalp.jpg'

const RootNotSelected = styled('div')(({ theme }) => ({
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  transition: '0.3s',
  width: '75%',
  transform: 'translateX(20%)',
  marginTop: '200px',
  padding: '20px',
  paddingBottom: '25px',
  display: 'block',
  textAlign: 'center',
  fontSize: '2rem',
}))

const Helper = styled('div')(({ theme }) => ({
  transition: '0.3s',
  width: '75%',
  transform: 'translateX(20%)',
  marginTop: '200px',
  padding: '20px',
  paddingBottom: '25px',
  display: 'block',
  textAlign: 'center',
  fontSize: '2rem',
}))

const Refugee = styled('div')(({ theme }) => ({
  transition: '0.3s',
  width: '75%',
  transform: 'translateX(20%)',
  marginTop: '200px',
  padding: '20px',
  paddingBottom: '25px',
  display: 'block',
  textAlign: 'center',
  fontSize: '2rem',
}))

export default function Home() {
  const { getAccessTokenSilently } = useAuth0()
  const [accessToken, setAccesToken] = useState()
  const [isUser, setIsUser] = useState({})
  const [userType, setUserType] = useState()

  useEffect(() => {
    if (typeof accessToken === 'undefined') {
      (async () => {
        const access_token = await getAccessTokenSilently();
        setAccesToken(access_token);
        fetch(`https://pwebgroup.tech:8080/api/is_user?access_token=` + access_token,
          {
            method: 'GET',
            dataType: 'json',
          }).then(function (response) {
            return response.json();
          }).then((actualData) => { setIsUser(actualData) })
      })();
    }
  }, [accessToken, getAccessTokenSilently]);

  const handleUserType = (type) => {
    setUserType(type.toLowerCase())
  }

  const onSendTypeHandle = () => {
    const jsonToSend = {access_token: accessToken, type: userType.toLowerCase()}
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=UTF-8' },
      body: JSON.stringify(jsonToSend)
    }
    fetch('https://pwebgroup.tech:8080/api/create_user_entry/', requestOptions)  
  }

  const getUserType = () => {
    if (typeof userType === 'undefined' && typeof accessToken !== 'undefined') {
      (async () => {
        fetch('https://pwebgroup.tech:8080/api/get_user_type/?access_token=' + accessToken,
        {
          method: 'GET',
          headers: { 
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
          },
        })
          .then(function (response) {return  response.json()})
          .then((actualData) => {setUserType(actualData.data)})
          .then(() => {return true})
      })()    
    }
  }

  return (
    isUser ?
      isUser.data === false
        ? <RootNotSelected>
          Are you a refugee or helper ? <SplitButton handleUserType={handleUserType} />
          <div>
            <Link to='/'><Button variant="contained" onClick={onSendTypeHandle}>Send <SendIcon style={{ marginLeft: '20px' }} /></Button></Link>
          </div>
        </RootNotSelected>
        : getUserType() || (userType === "helper" || userType === "Helper")
          ? <Helper><img src={HomeWalp} alt="Logo" /></Helper>
          : <Refugee><img src={HomeWalp} alt="Logo" /></Refugee> 
      : <div></div>
  )
}