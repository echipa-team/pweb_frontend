import React from "react";
import loading from "../../assets/loading.svg";

const Loading = () => (
  <div className="spinner">
    <img src={loading} alt="Loading" style={{marginTop: '350px', marginLeft: '85  0px'}}/>
  </div>
);

export default Loading;