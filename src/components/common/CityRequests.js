import React, { useEffect, useState } from "react"
import { useAuth0 } from "@auth0/auth0-react"

export default function CityRequests() {
  const { getAccessTokenSilently } = useAuth0()
  const [accessToken, setAccesToken] = useState()
  const [cities, setCities] = useState()
  const [details, setDetails] = useState()

  useEffect(() => {
    if (typeof accessToken === 'undefined') {
      (async () => {
        const access_token = await getAccessTokenSilently();
        setAccesToken(access_token);
        const requestOptions = {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        }
        await fetch('https://pwebgroup.tech:8080/api/get_subscription_details?access_token=' + access_token, requestOptions)
          .then(function (response) { return response.json() })
          .then((actualData) => {
            setDetails(actualData.data)
          })
      })();
    }
  }, [accessToken, getAccessTokenSilently]);

  const getRequestsFromCity = () => {
    if (typeof cities === 'undefined' && typeof details !== 'undefined' ) {
      (async () => {
        fetch(`https://pwebgroup.tech:8080/api/get_requests_from_city?city=/` + details.city,
          {
            method: 'GET',
            dataType: 'json',
          }).then(function (response) {
            return response.json();
          }).then((actualData) => { setCities(actualData.data) })
      })()
    }
  }
  return (
    getRequestsFromCity() || (cities && Object.keys(cities).length === 0)
    ? <div>No request in your city</div>
    : <div></div> 
  )
}