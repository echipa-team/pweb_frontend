import React, { useState, useEffect } from 'react'
import { styled } from '@mui/material/styles'
import { TextField, Button } from '@mui/material'
import SendIcon from '@mui/icons-material/Send'
import LocalOfferIcon from '@mui/icons-material/LocalOffer'
import { useAuth0 } from "@auth0/auth0-react"
import { Link } from 'react-router-dom';

const Root = styled('div')(({ theme }) => ({
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  transition: '0.3s',
  width: '25%',
  transform: 'translateX(150%)',
  marginTop: '100px',
  padding: '20px',
  paddingBottom: '50px',
  display: 'block',
}))

const boxesStyle = {
  marginTop: '15px',
  marginBottom: '15px',
}

const Title = styled('div')(({ theme }) => ({
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  transition: '0.3s',
  width: '75%',
  height: '75px',
  transform: 'translate(20%, -70%)',
  textAlign: 'center',
  fontSize: '25px',
  color: '#F5F0E7',
  background: 'linear-gradient(90deg, rgba(209,55,242,0.6615021008403361) 0%, rgba(195,49,213,0.6615021008403361) 33%, rgba(179,107,237,0.6502976190476191) 50%, rgba(40,151,205,0.5914740896358543) 66%, rgba(5,217,230,0.7399334733893557) 100%)',
  borderRadius: '20px',
  paddingTop: '15px',
}))

const sendBtnStyle = {
  display: 'block',
  height: '50px',
  width: '50%',
  fontSize: '2rem',
  background: 'linear-gradient(90deg, rgba(209,55,242,0.6615021008403361) 0%, rgba(195,49,213,0.6615021008403361) 33%, rgba(179,107,237,0.6502976190476191) 50%, rgba(40,151,205,0.5914740896358543) 66%, rgba(5,217,230,0.7399334733893557) 100%)',
  marginTop: '20px',
  transform: 'translateX(50%)',
  borderRadius: '20px',
  paddingTop: '0px',
}

export default function CreateOffer() {
  const { getAccessTokenSilently } = useAuth0()
  const [slotsValue, setSlotsValue] = useState('')
  const [cityValue, setCityValue] = useState('')
  const [dateTimeValue, setDateTimeValue] = useState('')
  const [addressValue, setAddressValue] = useState('')
  const [accessToken, setAccesToken] = useState()

  useEffect(() => {
    if (typeof accessToken === 'undefined') {
      (async () => {
        const access_token = await getAccessTokenSilently();
        setAccesToken(access_token);
      })();
    }
  }, [accessToken, getAccessTokenSilently]);

  const onSlotsValueChange = (event) => {
    console.log(event.target)
  }

  const onCityValueChange = (event) => {
    setCityValue(event.target.value)
  }

  const onAddressValueChange = (event) => {
    setAddressValue(event.target.value)
  }

  const onDateTimeValueChange = (event) => {
    console.log(event.target)
  }

  const onSendHandle = () => {
    const jsonToSend = { "access_token": accessToken, "slots": slotsValue, "city": cityValue, "address": addressValue, "date": new Date() }
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=UTF-8' },
      body: JSON.stringify(jsonToSend)
    }
    fetch('https://pwebgroup.tech:8080/api/create_help_location/', requestOptions)
  }

  return (
    <Root>
      <Title><LocalOfferIcon style={{ height: '30px', width: '30px', paddingBottom: '5px' }} /> Create Offer</Title>
      <TextField id="outlined-basic" label="Slots Number"
        variant="outlined" style={boxesStyle} onChange={onSlotsValueChange}
        value={slotsValue}
      />

      <TextField id="outlined-basic" label="City" variant="outlined"
        style={boxesStyle} onChange={onCityValueChange}
        value={cityValue}
      />

      <TextField id="outlined-basic" label="Address" variant="outlined"
        style={boxesStyle} onChange={onAddressValueChange}
        value={addressValue}
      />
      <div>DATE PICKER</div>
      <Link to="/offers" style={{textDecoration: 'none'}}><Button variant="contained" style={sendBtnStyle} onClick={onSendHandle}>Send <SendIcon /></Button></Link>
    </Root>
  )
}