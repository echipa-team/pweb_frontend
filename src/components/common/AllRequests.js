import React, { useEffect, useState } from "react"
import { useAuth0 } from "@auth0/auth0-react"

export default function CityRequests() {
  const { getAccessTokenSilently } = useAuth0()
  const [accessToken, setAccesToken] = useState()
  const [cities, setCities] = useState()
  
  useEffect(() => {
    if (typeof accessToken === 'undefined') {
      (async () => {
        const access_token = await getAccessTokenSilently();
        setAccesToken(access_token);
        fetch(`https://pwebgroup.tech:8080/api/get_all_requests/`,
          {
            method: 'GET',
            dataType: 'json',
          }).then(function (response) {
            return response.json();
          }).then((actualData) => { setCities(actualData.data) })
      })();
    }
  }, [accessToken, getAccessTokenSilently]);

  return (
    cities && Object.keys(cities).length === 0 
    ? <div> No requests </div>
    : <div> IMD </div>
  )
}