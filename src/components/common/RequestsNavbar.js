import React, { useState } from 'react'
import { Navbar, Nav, Container } from 'react-bootstrap'
import { styled } from '@mui/material/styles'

import AllRequests from '../common/AllRequests'
import CityRequests from '../common/CityRequests'

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

const navbarStyle = {
  height: '125px',
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  marginTop: '10px',
}

const Helper = styled('div')(({ theme }) => ({
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  transition: '0.3s',
  width: '75%',
  transform: 'translateX(20%)',
  marginTop: '200px',
  padding: '20px',
  paddingBottom: '25px',
  display: 'block',
  textAlign: 'center',
  fontSize: '2rem',
}))

export default function RequestsNavbar(props) {
  const [allOrCity, setAllOrCity] = useState(0)

  const setAll = () => {
    setAllOrCity(0)
  }

  const setCity = () => {
    setAllOrCity(1)
  }

  return (
    <Helper>
      <Navbar variant="light" bg="grey" expand="lg" style={navbarStyle}>
        <Container fluid>
          <Navbar.Toggle aria-controls="navbar-dark-example" />
          <Navbar.Collapse id="navbar-dark-example">
            <Nav>
              <h1 onClick={setAll} style={{ cursor: 'pointer', paddingLeft: '500px',  color: '#0000EE', textDecoration: 'underline' }}>All</h1>
              <h1 onClick={setCity} style={{ cursor: 'pointer', marginLeft: '100px', color: '#0000EE', textDecoration: 'underline'}}>From City</h1>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      {allOrCity === 0 
      ? <AllRequests></AllRequests>
      : <CityRequests></CityRequests>
      }
    </Helper>
  )
}