import React from 'react'
import { useAuth0 } from "@auth0/auth0-react"
import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import logo from '../../assets/sigla_pw.png'

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';

const navbarStyle = {
  height: '125px',
  boxShadow: '0 4px 8px 0 rgba(0,0,0,0.2)',
  marginTop: '10px',
}

const rightNavStyle = {
  marginLeft: '50%',
  marginRight: '10px',
}

const logoStyle = {
  height: '75px',
  width: '175px',
  marginRight: '30px'
}

const linksStyle = {
  fontSize: '2rem',
  marginTop: '10px',
  marginLeft: '40px'
}

const linksDropDownStyle = {
  fontSize: '1rem',
}

export default function NavBar(props) {
  const { logout } = useAuth0()

  return (
    <Navbar variant="light" bg="grey" expand="lg" style={navbarStyle}>
      <Container fluid>
        <Navbar.Toggle aria-controls="navbar-dark-example" />
        <Navbar.Collapse id="navbar-dark-example">
          <Nav>
            <img src={logo} alt="Logo" style={logoStyle} />
            <LinkContainer to="/" style={linksStyle}><Nav.Link>Home</Nav.Link></LinkContainer>
            <NavDropdown
              id="nav-dropdown-dark-example"
              title="Help"
              menuVariant="light"
              style={linksStyle}
            >
              <LinkContainer to="/offers" style={linksDropDownStyle}><Nav.Link>Offers</Nav.Link></LinkContainer>
              <LinkContainer to="/requests" style={linksDropDownStyle}><Nav.Link>Requests</Nav.Link></LinkContainer>
            </NavDropdown>
          </Nav>
          <Nav style={rightNavStyle}> 
            <LinkContainer to="/profile" style={linksStyle}><Nav.Link>Profile</Nav.Link></LinkContainer>
            <LinkContainer to="/" onClick={() => logout({returnTo: window.location.origin})} style={linksStyle}><Nav.Link>Logout</Nav.Link></LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}