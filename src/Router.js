import React, { useEffect } from "react"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import { useAuth0 } from "@auth0/auth0-react"
import NavBar from "./components/common/NavBar"
import Home from "./components/Routes/Home"
import Requests from "./components/Routes/Requests"
import Offers from "./components/Routes/Offers"
import CreateOffer from "./components/common/CreateOffer"
import Profile from "./components/Routes/Profile"
import CreateRequest from "./components/common/CreateRequest"

const Router = () => {
  const { isAuthenticated, loginWithRedirect } = useAuth0();

  useEffect(() => {
    if (!isAuthenticated) {
      loginWithRedirect();
    }
  }, [isAuthenticated, loginWithRedirect]);

  return (
    isAuthenticated && (
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/requests" element={<Requests />} />
          <Route exact path="/offers" element={<Offers />} />
          <Route exact path='/offers/create_offer' element={<CreateOffer />} />
          <Route exact path='/offers/create_request' element={<CreateRequest />} />
          <Route exact path="/profile" element={<Profile />} />
        </Routes>
      </BrowserRouter>
    )
  );
};

export default Router;